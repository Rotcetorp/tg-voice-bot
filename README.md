This telegram bot:
- decrypts your voice and video messages
- generates a text extension of your messages
- vocalises your text messages via gTTS (Google Text-to-Speech) synthesizer