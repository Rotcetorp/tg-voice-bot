import telebot
from wit import Wit
import ffmpeg
import requests
from gtts import gTTS
import os

tg_token = ''
bot = telebot.TeleBot(tg_token)

wit_token = ''
client = Wit(wit_token)


@bot.message_handler(content_types=['voice'])
def voice_decoder(message):
    try:
        message_id = message.chat.id
        file_info = bot.get_file(message.voice.file_id)
        downloaded_file = bot.download_file(file_info.file_path)
        file_name = f'{message_id}_{message.date}'
        with open(f'{file_name}.ogg', 'wb') as new_file:
            new_file.write(downloaded_file)
        conversion = ffmpeg.input(f'{file_name}.ogg')
        conversion = ffmpeg.output(conversion, f'{file_name}.wav')
        ffmpeg.run(conversion)
        with open(f'{file_name}.wav', 'rb') as f:
            resp = client.speech(f, {'Content-Type': 'audio/wav'})
        response_text = resp['text']
        text_ = send_request(response_text)
        text_to_speech(message, text_)

    except Exception:
        bot.reply_to(message, "Не удалось распознать сообщение.")
        raise


@bot.message_handler(content_types=['video_note'])
def video_note_decoder(message):
    try:
        message_id = message.chat.id
        file_info = bot.get_file(message.video_note.file_id)
        downloaded_file = bot.download_file(file_info.file_path)
        file_name = f'{message_id}_{message.date}'
        with open(f'{file_name}.mp4', 'wb') as new_file:
            new_file.write(downloaded_file)
        conversion = ffmpeg.input(f'{file_name}.mp4')
        conversion = ffmpeg.output(conversion, f'{file_name}.wav')
        ffmpeg.run(conversion)
        with open(f'{file_name}.wav', 'rb') as f:
            resp = client.speech(f, {'Content-Type': 'audio/wav'})
        response_text = resp['text']
        text_ = send_request(response_text)
        text_to_speech(message, text_)

    except Exception:
        bot.reply_to(message, "Не удалось распознать сообщение.")
        raise


def send_request(text: str) -> str:
    api_url = 'https://api.aicloud.sbercloud.ru/public/v1/public_inference/gpt3/predict'
    post_dict = {'text': text}
    r = requests.post(api_url, json=post_dict)
    predictions = r.json()["predictions"]
    if len(predictions) > 1000:
        predictions = predictions[:1000]
    return predictions



@bot.message_handler(content_types=['text'])
def send_generated_text(message):
    text = message.text
    response_text = send_request(text)
    bot.reply_to(message, response_text)


def text_to_speech(message, text_):
    message_id = message.chat.id
    myText = text_
    #  myText = text_.replace("\n", " ")
    tts = gTTS(text=myText, lang='ru', slow=False)
    tts.save('tts.mp3')
    audio = open('tts.mp3', 'rb')
    bot.send_audio(message_id, audio)
    #  os.remove(f"output.mp3")


if __name__ == '__main__':
    bot.polling(none_stop=True, interval=0)
